const express = require('express');
const helmet = require('helmet');
const path = require('path');
const mpdClient = require('./modules/mpd-client');
const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

let _client;

app.use(helmet());
app.use(express.urlencoded({ extended: false }));
app.use(async (req, res, next) => {
    if (!_client) {
        _client = await mpdClient();
    }
    const status = await _client.api.status.get();
    res.locals.mpdClient = _client;
    res.locals.status = status;
    next();
});

// views
app.get('/', async (req, res) => {
    console.log(res.locals.status);
    const search = `(any contains "${req.query.search}")`;
    const songs = req.query.search
        ? await res.locals.mpdClient.api.db.search(search)
        : await res.locals.mpdClient.api.queue.info();

    res.render('index', { songs, searchValue: req.query.search });
});

// commands
app.post('/dequeue', async (req, res) => {
    await res.locals.mpdClient.api.queue.deleteid(req.body.id);
    res.redirect('/');
});

app.post('/play', async (req, res) => {
    if (res.locals.status.state === 'pause') {
        await res.locals.mpdClient.api.playback.resume();
    }
    await res.locals.mpdClient.api.playback.play();
    res.redirect('/');
});

app.post('/queue', async (req, res) => {
    await res.locals.mpdClient.api.queue.add(req.body.file);
    res.redirect('/');
});

app.post('/stop', async (req, res) => {
    await res.locals.mpdClient.api.playback.stop();
    res.redirect('/');
});

app.listen(3000, () => {
    console.log('server started.');
});
