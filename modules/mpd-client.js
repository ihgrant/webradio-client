const dotenv = require('dotenv');
const mpdapi = require('mpd-api');

dotenv.config();

let _client;

module.exports = async function mpdClient() {
    if (!_client) {
        _client = await mpdapi.connect({
            host: process.env.HOST,
            port: process.env.PORT
        });
    }

    return _client;
};
