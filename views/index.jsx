const React = require('react');
const Song = require('./components/song');
const Controls = require('./components/controls');

function Index(props) {
    const isQueue = !props.searchValue || props.searchValue.length === 0;
    return (
        <div>
            <Controls playing={props.status.state === 'play'} />
            <form>
                <input
                    autoFocus={!isQueue}
                    defaultValue={props.searchValue}
                    name="search"
                    placeholder="Search..."
                />
                <input type="submit" />
            </form>
            <table style={{ width: '100%' }}>
                <tbody>
                    {props.songs.length > 0 ? (
                        props.songs.map((el, i) => (
                            <Song {...el} index={i} isQueue={isQueue} />
                        ))
                    ) : (
                        <tr>
                            <td>{`No results matching "${props.searchValue}" were found.`}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
}

module.exports = Index;
