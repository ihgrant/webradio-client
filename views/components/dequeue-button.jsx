const React = require('react');

module.exports = function DequeueButton(props) {
    return (
        <form action="/dequeue" method="post">
            <input type="hidden" name="id" value={props.id} />
            <button>{'-'}</button>
        </form>
    );
};
