const React = require('react');

module.exports = function Controls(props = { playing: false }) {
    const inlineBlock = { display: 'inline-block' };
    return (
        <div>
            <form action="/previous" method="post" style={inlineBlock}>
                <button>⏪</button>
            </form>
            {props.playing ? (
                <form action="/pause" method="post" style={inlineBlock}>
                    <button>⏸</button>
                </form>
            ) : (
                <form action="/play" method="post" style={inlineBlock}>
                    <button>▶️</button>
                </form>
            )}
            <form action="/stop" method="post" style={inlineBlock}>
                <button>⏹</button>
            </form>
            <form action="/next" method="post" style={inlineBlock}>
                <button>⏩</button>
            </form>
        </div>
    );
};
