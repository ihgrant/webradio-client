const React = require('react');

module.exports = function QueueButton(props) {
    return (
        <form action="/queue" method="post">
            <input type="hidden" name="file" value={props.file} />
            <button>{'+'}</button>
        </form>
    );
};
