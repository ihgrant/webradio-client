const React = require('react');
const DequeueButton = require('./dequeue-button');
const QueueButton = require('./queue-button');

module.exports = function Song(props) {
    return (
        <tr key={props.index}>
            <td>{props.index + 1}</td>
            <td>{props.artist}</td>
            <td>{props.album}</td>
            <td>{props.track}</td>
            <td>{props.title}</td>
            <td>
                {props.isQueue ? (
                    <DequeueButton id={props.id} />
                ) : (
                    <QueueButton file={props.file} />
                )}
            </td>
        </tr>
    );
};
